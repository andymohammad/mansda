<?php


Route::get('/', function () {
    return view('dashboard');
});

Route::prefix('admin')->group(function(){
    Route::get('profil-sekolah', 'MasterData@index_sekolah')->name('ProfilSekolah');
    Route::get('masterkelas', 'MasterData@index_kelas')->name('MasterKelas');
    Route::get('mastersiswa', 'MasterData@index_siswa')->name('MasterSiswa');
    Route::get('exp-kelas', 'MasterData@exp_kelas')->name('ExportKelas');
    Route::post('import-kelas', 'MasterData@imp_kelas')->name('ImportKelas');
    Route::get('exp-siswa', 'MasterData@exp_siswa')->name('ExportSiswa');
    Route::post('import-siswa', 'MasterData@imp_siswa')->name('ImportSiswa');

    Route::get('peminjaman', 'PeminjamanCon@index')->name('index-pinjam');
    Route::post('Peminjaman/post', 'PeminjamanCon@store')->name('store-pinjam');
    Route::post('peminjaman/kembali', 'PeminjamanCon@kembali')->name('pinjam-kembali');
    Route::get('peminjaman/kelas', 'PeminjamanCon@getsiswa')->name('getsiswa');
    
    Route::get('peminjaman/a', 'PeminjamanCon@coba');
});
