<div class="btn-group" role="group">
    <a href="javascript:void(0)" data-toggle="tooltip" data-id="{{ $id }}" data-original-tittle="Edit" class="edit btn btn-sm btn-success edit-user"><i class="fa fa-edit"></i></a>
    
    <a href="javascript:void(0);" id="deleteSiswa" data-toggle="tooltip" data-original-tittle="Delete" data-id="{{ $id }}" class="delete btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>
</div>