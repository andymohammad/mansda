@extends('layouts.core')
@section('style')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{ asset('DataTables-1.10.18/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{ asset('Responsive-2.2.2/css/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{ asset('select2/css/select2.min.css')}}">
<link rel="stylesheet" href="{{ asset('daterangepicker/daterangepicker.css')}}">
@endsection

@section('konten')
<div class="card card-success card-outline">
    <div class="card-header">
        <h3 class="card-title">Daftar Peminjaman Raport Siswa</h3>
        
        <div class="card-tools">
            <div class="btn-group btn-xs">
                <button type="button" class="btn btn-block btn-outline-success btn-sm" data-toggle="modal" data-target="#addPinjam">
                    <i class="fas fa-plus-circle"></i>
                </button>
                <a class="btn btn-block btn-outline-success btn-sm" href="#">
                    <i class="fas fa-cloud-download-alt"></i>
                </a>
            </div>
            <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    
    <div class="card-body">
        
        <div class="card-body table-responsive p-0">
            <table id="example" class="table table-striped table-bordered dt-responsive nowrap tabel" style="width:100%">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nama Siswa - Nomor Induk</th>
                        <th>Jurusan - Kelas</th>
                        <th>Keterangan</th>
                        <th>Tgl Pinjam</th>
                        <th>Tgl Kembali</th>
                        <th>Status</th>
                        <th>Walas</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($peminjaman as $a)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$a->siswa->nama}} <br> {{$a->nisn}}</td>
                        <td>
                            @if ($a->kelas->jurusan == 1)
                            {{ "IPA" }}
                            @else
                            {{ "IPS" }}
                            @endif
                            <br> 
                            {{$a->kelas->nama_kelas}}
                        </td>
                        <td>{{$a->keterangan}}</td>
                        
                        <td>{{\Carbon\Carbon::parse($a->tgl_pinjam)->format('d - m - Y')}}</td>
                        <td>{{\Carbon\Carbon::parse($a->tgl_kembali)->format('d - m - Y')}}</td>
                        <td>
                            @if ($a->status > 0)
                            <span class="badge badge-info right">Dipinjam</span>
                            @else
                            <span class="badge badge-success right">Kembali</span>
                            @endif
                        </td>
                        <td>{{$a->kelas->walas}}</td>
                        <td>
                            @if ($a->status > 0)
                            <form action="{{ route('pinjam-kembali')}}" method="POST">
                                {{ csrf_field() }}
                                <input type="hidden" name="statusPinjam" value="{{$a->nisn}}" />
                                <button href="" id="kembali" class="btn btn-block btn-sm btn-danger kembali" data-nisn={{$a->nisn}}>
                                    Kembali
                                </button>
                            </form>
                            @else
                            <a href="#" class="btn btn-block btn-sm disabled btn-success">Clear</a>
                            @endif
                            
                        </td>
                    </tr>
                    @endforeach                    
                </tbody>
            </table>
        </div>
        
    </div>
    
</div>
<div class="modal fade" id="addPinjam" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="formPeminjaman" method="POST" action="{{route('store-pinjam')}}" class="form-horizontal">
                {{ csrf_field() }}
                <div class="modal-header">
                    <h4 class="modal-title">Input Data Peminjaman</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card-body">
                        
                        <div class="form-group row">
                            <label class="col-sm-4 col-xs-12 control-label">Kode Kelas</label>
                            <div class="col-sm-8 col-xs-12">
                                <select name="kode_kelas" id="kode_kelas" data-dependency="nama" class="form-control select2 kode" style="width: 100%;">
                                    @foreach ($kelas as $a)
                                    <option id="kode_kelas" value="{{$a->kode_kelas}}">{{$a->nama_kelas}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label class="col-sm-4 col-xs-12 control-label">Nama Siswa</label>
                            <div class="col-sm-8 col-xs-12">
                                <select name="nama" id="nama" class="form-control select2">
                                    @foreach ($siswa as $a)
                                    <option id="nama" value="{{$a->nisn}}">{{$a->nama}} - {{$a->nisn}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label class="col-sm-4 col-xs-12 control-label">Durasi Peminjaman</label>
                            <div class="col-sm-8 col-xs-12">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="far fa-calendar-alt"></i>
                                        </span>
                                    </div>
                                    <input type="text" name="tanggal" class="form-control float-right" id="tanggal">
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label class="col-sm-4 col-xs-12 control-label">Keterangan</label>
                            <div class="col-sm-8 col-xs-12">
                                <textarea class="form-control" name="keterangan" id="keterangan" rows="3" placeholder="Keperluan Peminjaman"></textarea>
                            </div>
                        </div>
                        
                    </div>
                    <!-- /.card-body -->
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button id="ajaxSubmit" type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endsection

@section('jskonten')


<script src="{{ asset('DataTables-1.10.18/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('DataTables-1.10.18/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{ asset('Responsive-2.2.2/js/dataTables.responsive.min.js')}}"></script>
<script src="{{ asset('Responsive-2.2.2/js/responsive.bootstrap4.min.js')}}"></script>

<script src="{{ asset('select2/js/select2.full.min.js')}}"></script>

<script src="{{ asset('inputmask/jquery.inputmask.bundle.js')}}"></script>
<script src="{{ asset('moment/moment.min.js')}}"></script>
<script src="{{ asset('moment/moment-with-locales.min.js')}}"></script>
<script src="{{ asset('daterangepicker/daterangepicker.js')}}"></script>


<script>
    $('#tanggal').daterangepicker();
    $('input[name="tanggal"]').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('YYYY-MM-DD') + ' - ' + picker.endDate.format('YYYY-MM-DD'));
    });
    
    $(document).ready( function () {
        $('#example').DataTable();
        
    } );
    
    $('#kode_kelas').on('change', function(e){
        console.log(e);
        var kode_kelas = e.target.value;
        $.get('/admin/peminjaman/kelas?kode_kelas=' + kode_kelas,function(data) {
            console.log(data);
            $('#nama').empty();
            
            
            $.each(data, function(index, siswaObj){
                $('#nama').append('<option value="'+ siswaObj.nisn +'">'+ siswaObj.nama + ' - ' + siswaObj.nisn +'</option>');
            })
        });
    });
    
</script>
@endsection