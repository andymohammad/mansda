@extends('layouts.core')

@section('style')
<link rel="stylesheet" href="{{ asset('datatables/dataTables.bootstrap4.css')}}">
@endsection

@section('konten')
<!-- Default box -->
<div class="card">
    
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Data Siswa</h3>
            
            <div class="card-tools">
                <div class="btn-group btn-xs">
                    <button type="button" class="btn btn-block btn-outline-success btn-sm" data-toggle="modal"
                    data-target="#addKelas">
                    <i class="fas fa-plus-circle"></i>
                </button>
                <button type="button" class="btn btn-block btn-outline-success btn-sm" data-toggle="modal"
                data-target="#upload">
                <i class="fas fa-cloud-upload-alt"></i>
            </button>
            <a class="btn btn-block btn-outline-success btn-sm" href="{{route('ExportSiswa')}}">
                <i class="fas fa-cloud-download-alt"></i>
            </a>
        </div>
        <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip"
        title="Collapse">
        <i class="fas fa-minus"></i></button>
        <button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fas fa-times"></i></button>
        </div>
    </div>
    <div class="card-body">
        
        <table id="TabelSiswa" class="table table-bordered table-hover">
            <thead style="text-align: center;">
                <tr>
                    <th style="width:2px;">#</th>
                    <th>Nama Siswa</th>
                    <th>Jurusan</th>
                    <th>Nama Kelas</th>
                    <th>Wali Kelas</th>
                    <th>Keterangan</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($siswa as $a)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $a->nama }}</td>
                    <td>
                        @if ($a->kelas->jurusan == 1 )
                        {{ 'IPA' }}
                        @else
                        {{'IPS'}}
                        @endif
                    </td>
                    <td>{{ $a->kelas->nama_kelas }}</td>
                    <td>{{ $a->kelas->walas }}</td>
                    <td style="text-align: center;"><span class="badge badge-success">Bebas Administrasi</span></td>
                    <td style="text-align: center;">
                        <a href="#" class="btn btn-sm btn-square bg-gradient-warning"><i
                            class="fas fa-edit"></i></a>
                            <a href="#" class="btn btn-sm btn-square bg-gradient-danger"><i
                                class="fas fa-trash"></i></a>
                            </td>
                            
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
            
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->
        
        
        
        {{-- MODAL TAMBAH --}}
        <div class="modal fade" id="addKelas" style="display: none;" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <form id="formKelas" class="form-horizontal">
                        <div class="modal-header">
                            <h4 class="modal-title">Tambah Data Kelas</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="card-body">
                                <div class="form-group row">
                                    <label class="col-sm-2 control-label">Kode Kelas</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="kode_kelas">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 control-label">Nama Kelas</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="nama_kelas">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="jurusan" class="col-sm-2 control-label">Jurusan</label>
                                    <div class="col-sm-10">
                                        <select name="jurusan" id="jurusan" class="form-control">
                                            <option value="1">IPA</option>
                                            <option value="2">IPS</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="walas" class="col-sm-2 control-label">Wali Kelas</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="walas">
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        
        <div class="modal fade" id="addSiswa" style="display: none;" aria-hidden="true">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <form id="formSiswa" class="form-horizontal">
                        <div class="modal-header">
                            <h4 class="modal-title">Tambah Data Siswa</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="card-body">
                                <div class="form-group row">
                                    <label class="col-sm-2 control-label">Kode Kelas</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="kode_kelas">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 control-label">Nama Kelas</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="nama_kelas">
                                    </div>
                                </div>
                                
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Upload</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        
        <div class="modal fade" id="upload" style="display: none;" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Upload Data</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <form action=" {{ route('ImportSiswa') }}" role="form" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="modal-body">
                            <div class="form-group">
                                <div class="callout callout-info">
                                    <h4>Template Siswa</h4>
                                    <p>download <a href="#" style="color: red" ;>disini</a></p>
                                </div>
                            </div>    
                            <div class="form-group">
                                <label for="exampleInputFile">File input</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" name="file" class="custom-file-input" id="exampleInputFile">
                                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        
        @endsection
        
        
        @section('jskonten')
        
        <script src="{{ asset('datatables/jquery.dataTables.js')}}"></script>
        <script src="{{ asset('datatables/dataTables.bootstrap4.js')}}"></script>
        <script>
            $(function () {
                $("#TabelSiswa").DataTable();

                $('body').on('click', '#deleteSiswa', function () {
                    
                    var idsiswa = $(this).data("id");
                    confirm("Apakah Anda Yakin ? ");
                    
                    $.ajax({
                        type: "get",
                        url: '{{ url('/admin/mastersiswa/delete') }}/'+idsiswa,
                        success: function (data) {
                            var oTable = $('#TabelSiswa').dataTable(); 
                            oTable.fnDraw(false);
                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });
                });Z
            });
        </script>
        @endsection