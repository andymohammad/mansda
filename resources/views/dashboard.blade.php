@extends('layouts.core')

@section('style')
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<script src="https://unpkg.com/ionicons@4.5.10-0/dist/ionicons.js"></script>
@endsection
@section('konten')
@php
$k = DB::table('kelas')->count();
$s = DB::table('siswa')->count();
$p = DB::table('peminjaman')->count();
$a = DB::table('peminjaman')->where('status', '=', 1)->count();

@endphp
<div class="container-fluid">
    <div class="callout callout-info">
        <h5>Selamat Datang</h5>

        <p>Selamat datang MAN SIDOARJO di Sistem Informasi Peminjaman Raport Siswa.</p>
    </div>
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Beranda</h3>

            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
                    <i class="fas fa-minus"></i>
                </button>
                <button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                    <i class="fas fa-times"></i>
                </button>
            </div>
        </div>
        <div class="card-body table-responsive p-0">
            <table class="table table-hover">
                <tbody>
                    <tr>
                        <td style="width:240px;">NSM</td>
                        <td>:</td>
                        <td><span class="tag tag-success">131135150001</span></td>
                    </tr>
                    <tr>
                        <td style="width:240px;">NPSN</td>
                        <td>:</td>
                        <td><span class="tag tag-success">20584616</span></td>
                    </tr>
                    <tr>
                        <td style="width:240px;">NAMA SEKOLAH</td>
                        <td>:</td>
                        <td><span class="tag tag-success">MADRASAH ALIYAH NEGERI SIDOARJO</span></td>
                        <td style="width:240px;">STATUS</td>
                        <td>:</td>
                        <td><span class="btn btn-block btn-sm btn-success">NEGERI</span></td>
                    </tr>
                    <tr>
                        <td style="width:240px;">JURUSAN</td>
                        <td>:</td>
                        <td><span class="tag tag-success">IPA - IPS</span></td>
                    </tr>
                    <tr>
                        <td style="width:240px;">ALAMAT SEKOLAH</td>
                        <td>:</td>
                        <td><span class="tag tag-success">JALAN STADION NO. 2 , SIDOARJO</span></td>
                    </tr>

                </tbody>
            </table>

        </div>
    </div>


    <div class="row">
        <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
                <div class="inner">
                    <h3>{{$k}}</h3>

                    <p>Kelas</p>
                </div>
                <div class="icon">
                    <i class="ion ion-bag"></i>
                </div>
                <a href="#" class="small-box-footer">Selengkapnya <i class="fas fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
                <div class="inner">
                    <h3>{{$s}}</h3>

                    <p>Siswa</p>
                </div>
                <div class="icon">
                    <i class="ion ion-people"></i>
                </div>
                <a href="#" class="small-box-footer">Selengkapnya <i class="fas fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
                <div class="inner">
                    <h3>{{$p}}</h3>

                    <p>Peminjaman</p>
                </div>
                <div class="icon">
                    <i class="ion ion-person-add"></i>
                </div>
                <a href="#" class="small-box-footer">Selengkapnya <i class="fas fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-danger">
                <div class="inner">
                    <h3>{{$a}}</h3>

                    <p>Belum Dikembalikan</p>
                </div>
                <div class="icon">

                    <i class="ion">
                        <ion-icon name="send"></ion-icon>
                    </i>
                </div>
                <a href="#" class="small-box-footer">Selengkapnya <i class="fas fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
    </div>
</div>
@endsection

@section('jskonten')
<script>

</script>
@endsection