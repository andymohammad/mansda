@extends('layouts.core')

@section('konten')
@php
$s = DB::table('siswa')->count();
@endphp

<div class="container-fluid">
    <div class="callout callout-info">
        <h5>Selamat Datang</h5>
        
        <p>Selamat datang MAN SIDOARJO di Aplikasi AbalAbal.</p>
    </div>
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Beranda</h3>
            
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i>
                </button>
                <button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                    <i class="fas fa-times"></i>
                </button>
            </div>
        </div>
        <div class="card-body" style="height:200px;">
        </div>
    </div>
    
    
    <div class="row">
        
    </div>
</div>
@endsection