<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Peminjaman extends Model
{
    protected $table = 'peminjaman';
    protected $fillable = [
        'nis', 'tgl_pinjam', 'tgl_kembali','walas'
    ];


    public function siswa(){
        return $this->belongsTo('App\Siswa', 'nisn','nisn');
    }

    public function kelas(){
        return $this->belongsTo('App\Kelas', 'kode_kelas','kode_kelas');
    }
}
