<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    protected $table = 'siswa';
    //protected $primaryKey = 'nisn';
    protected $fillable = [
        'nisn', 'nis', 'nama', 'kode_kelas', 'notelp'
    ];

    public function peminjaman(){
        return $this->hasOne('App\Peminjaman', 'nisn','nisn');
    }

    public function kelas(){
        return $this->belongsTo('App\Kelas', 'kode_kelas', 'kode_kelas');
    }
}
