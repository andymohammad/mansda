<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;

use Illuminate\Http\Request;
use App\Peminjaman;
use App\Siswa;
use App\Kelas;
use DataTables;
use DB;

class PeminjamanCon extends Controller
{
    public function index()
    {
        
        $siswa = Siswa::all();
        $kelas = Kelas::all();
        $peminjaman = Peminjaman::orderBy('status', 'DESC')->get();
        return view('pinjam', compact('siswa', 'kelas', 'peminjaman'));
        
    }
    
    public function create()
    {
        //
    }
    
    public function kembali(Request $request)
    {
        
        $nisn = $request->input('statusPinjam');
        $siswa = Peminjaman::where('nisn', '=', $nisn)->first();
        $siswa->status = 0;
        $siswa->update();
        
        return redirect()->back()->with('success', 'Raport Sudah Dikembalikan');
    }
    
    public function getsiswa()
    {
        
        $kode_kelas = Input::get('kode_kelas');
        $siswa = Siswa::where('kode_kelas', '=', $kode_kelas)->get();
        return response()->json($siswa);
    }

    public function coba()
    {
        return view('a');
    }
    
    
    public function store(Request $request)
    {
        $tgl = $request->tanggal;
        $tgl1 = explode(' - ', $tgl);
        $tgl_pinjam = $tgl1[0];
        $tgl_kembali = $tgl1[1];
        
        $peminjaman = new Peminjaman();
        $peminjaman->nisn = $request->nama;
        $peminjaman->tgl_pinjam = $tgl_pinjam;
        $peminjaman->tgl_kembali = $tgl_kembali;
        $peminjaman->keterangan = $request->keterangan;
        $peminjaman->kode_kelas = $request->kode_kelas;
        $peminjaman->save();
        
        return redirect()->back();
    }
    
    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        //
    }
    
    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        //
    }
    
    
    
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        //
    }
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        //
    }
}
