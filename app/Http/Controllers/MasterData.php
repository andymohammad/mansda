<?php

namespace App\Http\Controllers;

use App\Exports\KelasExport;
use App\Exports\SiswaExport;
use App\Imports\KelasImport;
use App\Imports\SiswaImport;

use Maatwebsite\Excel\Facades\Excel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Kelas;
use App\Siswa;
use Redirect,Response;

class MasterData extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index_sekolah()
    {
        return view('sekolah');
    }
    public function index_kelas()
    {
        $kelas = Kelas::all();
        return view('kelas', compact('kelas'));
        //$data['kelas'] = Kelas::orderBy('kode_kelas','desc');
        //return view('siswa',$data);
    }
    
    public function index_siswa()
    {
        $siswa = Siswa::all();
        
        return view('siswa', compact('siswa'));
        //$data['kelas'] = Kelas::orderBy('kode_kelas','desc');
        //return view('siswa',$data);
    }
    
    public function exp_kelas()
    {
        return Excel::download(new KelasExport, 'kelas.xlsx');
    }
    public function imp_kelas(Request $request)
    {
        $this->validate($request, [
            'file' => 'required|mimes:xls,xlsx'
            ]);

            if ($request->hasFile('file')) {
                $file = $request->file('file'); //GET FILE
                Excel::import(new KelasImport, $file); //IMPORT FILE 
                return redirect()->back()->with(['success' => 'Upload Sukses']);
            }

            return redirect()->back()->with(['error' => 'Silahkan Pilih File'])->withInput();
        }
        
        public function exp_siswa()
        {
            return Excel::download(new SiswaExport, 'data_siswa.xlsx');
        }
        
        public function imp_siswa(Request $request)
        {
            $this->validate($request, [
                'file' => 'required|mimes:xls,xlsx'
                ]);
    
                if ($request->hasFile('file')) {
                    $file = $request->file('file'); //GET FILE
                    Excel::import(new SiswaImport, $file); //IMPORT FILE 
                    return redirect()->back()->with(['success' => 'Upload Sukses']);
                }
    
                return redirect()->back()->with(['error' => 'Silahkan Pilih File'])->withInput();
            }
        /**
        * Show the form for creating a new resource.
        *
        * @return \Illuminate\Http\Response
        */
        public function create()
        {
            //
        }
        
        /**
        * Store a newly created resource in storage.
        *
        * @param  \Illuminate\Http\Request  $request
        * @return \Illuminate\Http\Response
        */
        public function store(Request $request)
        {
            //
        }
        
        /**
        * Display the specified resource.
        *
        * @param  int  $id
        * @return \Illuminate\Http\Response
        */
        public function show($id)
        {
            //
        }
        
        /**
        * Show the form for editing the specified resource.
        *
        * @param  int  $id
        * @return \Illuminate\Http\Response
        */
        public function edit($id)
        {
            //
        }
        
        /**
        * Update the specified resource in storage.
        *
        * @param  \Illuminate\Http\Request  $request
        * @param  int  $id
        * @return \Illuminate\Http\Response
        */
        public function update(Request $request, $id)
        {
            //
        }
        
        /**
        * Remove the specified resource from storage.
        *
        * @param  int  $id
        * @return \Illuminate\Http\Response
        */
        public function destroy($id)
        {
            //
        }
    }
    