<?php

namespace App\Imports;

use App\Kelas;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class KelasImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Kelas([
            'kode_kelas'  => $row['kode_kelas'],
            'nama_kelas' => $row['nama_kelas'],
            'jurusan'    => $row['jurusan'],
            'walas'    => $row['wali_kelas'],
            ]);
        }
    }
    