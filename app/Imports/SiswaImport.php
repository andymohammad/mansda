<?php

namespace App\Imports;

use App\Siswa;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;


class SiswaImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Siswa([
            
            'nisn' => $row['nisn'],
            'nis'    => $row['nis'],
            'nama'    => $row['nama_siswa'],
            'kode_kelas'=> $row['kode_kelas'],
            'notelp'    => $row['nomor_telepon'],
            
        ]);
    }
}
