<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kelas extends Model
{
    protected $table = 'kelas';
    protected $primarykey = 'kode_kelas';
    protected $fillable = [
        'kode_kelas', 'nama_kelas', 'jurusan', 'walas'
    ];
    
    public function peminjaman(){
        return $this->hasMany('App\Peminjaman', 'walas', 'kode_kelas');
    }
    public function siswa(){
        return $this->hasMany('App\Siswa', 'kode_kelas');
    }
    
}
