<?php

namespace App\Exports;

use App\Kelas;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class KelasExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Kelas::all();
    }
    public function headings(): array
    {
        return [
            '#',
            'Kode Kelas',
            'Nama Kelas',
            'Jurusan',
            'Wali Kelas'
        ];
    }
}
