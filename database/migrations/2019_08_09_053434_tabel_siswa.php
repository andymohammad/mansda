<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TabelSiswa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siswa', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nisn', 20)->unique();
            $table->string('nis', 20)->unique();
            $table->string('nama');
            $table->string('kode_kelas', 10);
            $table->foreign('kode_kelas')
            ->references('kode_kelas')->on('kelas')->onDelete('cascade')->onUpdate('cascade');
            $table->string('notelp')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('siswa');
    }
}
